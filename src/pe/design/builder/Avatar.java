package pe.design.builder;

public final class Avatar {
    private String name;
    private HairType hairType;
    private HairColour hairColour;
    private SkinTone skinTone;

    public Avatar(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HairType getHairType() {
        return hairType;
    }

    public void setHairType(HairType hairType) {
        this.hairType = hairType;
    }

    public HairColour getHairColour() {
        return hairColour;
    }

    public void setHairColour(HairColour hairColour) {
        this.hairColour = hairColour;
    }

    public SkinTone getSkinTone() {
        return skinTone;
    }

    public void setSkinTone(SkinTone skinTone) {
        this.skinTone = skinTone;
    }

    @Override
    public String toString(){
        return "[name " + this.name
                + ", hair type " + this.hairType
                + ", hair colour " + this.hairColour
                + ", skin tone " + this.skinTone
                + "] ";
    }
}
