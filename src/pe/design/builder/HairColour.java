package pe.design.builder;

public enum HairColour {
    SILVER, CARAMEL, ROSE, ASH
}
