package pe.design.builder;

public enum SkinTone {
    PALE, ROSY_PALE, LIGHT, NORMAL, TAN, EXOTIC, MEDIUM, DARK, NATIVE
}
