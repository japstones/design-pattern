package pe.design.builder;

public class AvatarBuilder {
    private Avatar avatar;

    public AvatarBuilder(String name) {
        this.avatar = new Avatar(name);
    }

    public AvatarBuilder withHairType(HairType hairType) {
        this.avatar.setHairType(hairType);
        return this;
    }

    public AvatarBuilder withHairColour(HairColour hairColour) {
        this.avatar.setHairColour(hairColour);
        return this;
    }

    public AvatarBuilder withSkinTone(SkinTone skinTone) {
        this.avatar.setSkinTone(skinTone);
        return this;
    }

    public Avatar build() {
        return this.avatar;
    }
}
