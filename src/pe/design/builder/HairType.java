package pe.design.builder;

public enum HairType {
    WAVY, CURLY, KINKY
}
