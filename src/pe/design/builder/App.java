package pe.design.builder;

public class App {
    public static void main(String ... arg) {
        Avatar avatar = new AvatarBuilder("Japstones")
                .withHairColour(HairColour.SILVER)
                .withHairType(HairType.CURLY)
                .withSkinTone(SkinTone.DARK)
                .build();

        System.out.println(avatar);
    }
}
