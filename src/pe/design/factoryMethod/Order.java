package pe.design.factoryMethod;

import java.math.BigDecimal;
import java.util.Date;

public class Order {
    private String client;
    private BigDecimal totalAmount;
    private String cardNumber;
    private String cardCode;
    private Date cardExpire;

    public Order(String client, BigDecimal totalAmount) {
        this.client = client;
        this.totalAmount = totalAmount;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public Date getCardExpire() {
        return cardExpire;
    }

    public void setCardExpire(Date cardExpire) {
        this.cardExpire = cardExpire;
    }
}
