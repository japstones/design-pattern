package pe.design.factoryMethod;

public class CashPayment implements Payment {
    @Override
    public boolean doPayment(Order order) {
        System.out.println("Cash payment by " + order.getClient());
        return false;
    }
}
