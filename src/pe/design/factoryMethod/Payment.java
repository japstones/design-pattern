package pe.design.factoryMethod;

public interface Payment {
    boolean doPayment(Order order);
}
