package pe.design.factoryMethod;

import java.math.BigDecimal;

public class App {
    public static void main(String... arg) {
        Payment payment = PaymentFactory.createPayment(PaymentType.CARD_PAYMENT);
        payment.doPayment(new Order("Japtones" , new BigDecimal(350)));

        payment = PaymentFactory.createPayment(PaymentType.CASH_PAYMENT);
        payment.doPayment(new Order("Rosa" , new BigDecimal(350)));
    }
}
