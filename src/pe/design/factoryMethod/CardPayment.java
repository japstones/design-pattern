package pe.design.factoryMethod;

public class CardPayment implements Payment {
    @Override
    public boolean doPayment(Order order) {
        System.out.println("Card payment by " + order.getClient());

        return validCardInfo(order);
    }

    private boolean validCardInfo(Order order){
        System.out.println("Validating card info ");
        return false;
    }
}
