package pe.design.factoryMethod;

public enum PaymentType {
    CASH_PAYMENT, CARD_PAYMENT
}