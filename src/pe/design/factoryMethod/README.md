**Factory Method**

El objetivo del patrón Factroy Method es proveer un método abstracto de 
creación de un objeto delegando en las subclases concretas su creación 
efectiva.

Proporcina una forma de delegar la lógica de creación de instancias a 
clases secundarias.

**Uso**

* Una clase que sólo conoce los objetos con lo que tiene que relaciones.
* Una clase quiere transmitir a sus subclases las elecciones de instanciación
aprovechando un mecanismo de polimorfismo.


