package pe.design.factoryMethod;

public abstract class PaymentFactory {

    public static Payment createPayment(PaymentType type) {
        switch (type) {
            case CARD_PAYMENT:
                return new CardPayment();
            case CASH_PAYMENT:
                return new CashPayment();
        }
        return null;
    }
}