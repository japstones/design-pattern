package pe.design.abstractFactory;

public class Tshirt {
    private String description;

    public Tshirt(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
