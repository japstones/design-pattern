package pe.design.abstractFactory;

public abstract class FactoryMaker {

    public enum ClothingType {
        POLYESTER, SYNTHETIC
    }

    public static ClothingFactory makeFactory(ClothingType type) {
        switch (type) {
            case POLYESTER:
                return new PolyesterFactory();
            case SYNTHETIC:
                return new SyntheticFactory();
            default:
                throw new IllegalArgumentException("ClotingType is not supported");
        }
    }
}
