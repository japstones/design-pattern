package pe.design.abstractFactory;

public interface ClothingFactory {
    Tshirt createTshirt();

    Pant createPant();
}
