package pe.design.abstractFactory;

public class App {
    public static void main(String... arg) {
        ClothingFactory factory = FactoryMaker.makeFactory(FactoryMaker.ClothingType.SYNTHETIC);
        Pant pant = factory.createPant();
        System.out.println(pant.getDescription());
    }
}
