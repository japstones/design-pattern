**Abstract Factory**

Tiene como objetivo la creación de objetos reagrupados en familias sin tener 
que conocer las clases concreatas destinadas a la creación de estos objetos.

**Uso**

* Un sistema que utiliza productos necesita ser independiente de la forma en que
se crean y agrupan estos productos.

* Un sistema está configurado según varias familias de productos que pueden 
evolucionar

