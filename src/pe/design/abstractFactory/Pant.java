package pe.design.abstractFactory;

public class Pant {
    private String description;

    public Pant(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
