package pe.design.abstractFactory;

public class SyntheticFactory implements ClothingFactory {
    public SyntheticFactory() {
    }

    @Override
    public Tshirt createTshirt() {
        return new Tshirt("Synthetic t-shirt");
    }

    @Override
    public Pant createPant() {
        return new Pant("Synthetic pant");
    }
}
