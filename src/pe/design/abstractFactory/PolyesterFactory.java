package pe.design.abstractFactory;

public class PolyesterFactory implements ClothingFactory {
    public PolyesterFactory() {
    }

    @Override
    public Tshirt createTshirt() {
        return new Tshirt("Polyester t-shirt");
    }

    @Override
    public Pant createPant() {
        return new Pant("Polyester pant");
    }
}
